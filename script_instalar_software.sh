sudo apt update
sudo apt upgrade -y
sudo apt install -y curl

#Añadimos repositorios necesarios.

# chromne
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'

# atom
curl -L https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'

# docker
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose


# oracle-java8
sudo add-apt-repository -y ppa:webupd8team/java

# Virtualbox
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -

sudo apt update

# #################################################################
# Instalamos las aplicaciones.
# #################################################################

sudo apt install -y kde-l10n-es firefox-locale-es msttcorefonts
sudo apt install -y kdegraphics-thumbnailers kde-thumbnailer-openoffice kffmpegthumbnailer
sudo apt install -y vim mc tmux git sshfs openssh-server synaptic
sudo apt install -y google-chrome-stable
sudo apt install -y nodejs npm
sudo apt install -y atom filezilla
sudo apt install -y krusader kdiff3 krename
sudo apt install -y inkscape gimp gimp-plugin-registry pdfsam

sudo apt install -y docker-ce
sudo groupadd docker
sudo usermod -aG docker $USER


curl -O https://www.syntevo.com/downloads/smartgit/smartgit-17_1_6.deb && sudo dpkg -i smartgit-17_1_6.deb

sudo apt install -y mysql-workbench

sudo apt-get purge openjdk*
sudo apt install -y oracle-java8-installer

curl -o http://bits.netbeans.org/netbeans/8.2/community/bundles/netbeans-8.2-php-linux-x64.sh && chmod +x netbeans-8.2-php-linux-x64.sh && sudo ./netbeans-8.2-php-linux-x64.sh
curl -o teamviewer_amd64.deb https://dl.tvcdn.de/download/linux/version_13x/teamviewer_13.1.3026_amd64.deb && sudo dpkg -i teamviewer_amd64.deb

sudo apt install -y virtualbox

sudo apt autoremove
